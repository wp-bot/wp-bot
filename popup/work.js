function saveWork () {
    $('#saveWorks').prop("disabled", true);
    $('#deleteWorks').removeAttr("disabled");
    var ranks = $("#work input[type='number']:not(#fid)");
    var works = [];
    for (let index = 0; index < ranks.length; index++) {
        var rank = $(ranks[index]);
        if(ranks[index].id != 'fid_rank'){            
            var parent = rank.parent();
            var checkbox = parent.find("input[type='checkbox']");
            var isChecked = checkbox[0].checked;
            if(isChecked){
                works.push({"type": Number(checkbox[0].name), 
                        "rank": Number($(ranks[index]).val()), "fid": 0});
            }
        }else{
            var checkbox = $("#work_on_factory");
            var isChecked = checkbox[0].checked;
            var factory_type= $("#factory_type").val();
            var fid = $("#fid").val();
            if(isChecked){
                works.push({"type": Number(factory_type), 
                        "rank": Number($(ranks[index]).val()), "fid": Number(fid)});
            }
        }
    }
    works = works.sort(function(a, b){
            if (a.rank > b.rank) {
                return 1;
            }
            if (a.rank < b.rank) {
                return -1;
            }
            return 0;
        });
    for (let i = 0; i < works.length; i++) {
        const element = works[i];
        works[i] = [element.type];
        if(element.fid){
            works[i].push(element.fid);
        }
    }
    var works_obj = {"type": "work", "list": works, "msg": "start"};
    chrome.runtime.sendMessage(works_obj,function(res)
    {
        if(!res){
            return console.log(chrome.runtime.lastError);
        }
    });  
}
function resetWorking(){
    $('#deleteWorks').prop("disabled", true);
    $('#saveWorks').removeAttr("disabled");
    chrome.runtime.sendMessage({"type": "work", msg:" stop"},function(res)
    {
        if(!res){
            return console.log(chrome.runtime.lastError);
        }
    }); 
}
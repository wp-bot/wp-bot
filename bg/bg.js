/**
 * Created by Root on 07.11.2015.
 */
////////////////////////// work //////////////////////////
function working(works, works_info, works_payd){
    var work = works[0];
    if(user.energy < works_info[work[0]-1].en){
            return;
    }
    works.shift();
    works.push(work);
    if(work.length==2){
        $.post('https://imperialism.online/api/Proc/?' + RandomGet(), { id: id, key: key, time_login: time_login, proc: "do_work", wid: work[0], fid:work[1]}, function (data) {
            var json = jQuery.parseJSON(data);
            if(json.r < 0)
            {
                return;
            }
            console.info("get: " + works_info[work[0]-1].profit + ' ' + works_payd[work[0]-1]);
            console.info("lost: " + works_info[work[0]-1].en + 'e');
        });
    }
    else{
        $.post('https://imperialism.online/api/Proc/?' + RandomGet(), { id: id, key: key, time_login: time_login, proc: "do_work", wid: work[0], fid:0}, function (data) {
            var json = jQuery.parseJSON(data);
            if(json.r < 0)
            {
                return;
            }
            console.info("get: " + works_info[work[0]-1].profit + ' ' + works_payd[work[0]-1]);
            console.info("lost: " + works_info[work[0]-1].en + 'e');
        });
    }
}
function get_works(){
    var works_info=[];
    $.post('api/Proc/?' + RandomGet(), { id: id, key: key, time_login: time_login, proc: "sel_work"}, function (data) {
        var json = jQuery.parseJSON(data);
        for(var i=0; i<json.work.length; i++)
        {
            works_info.push({"en": json.work[i].en, "profit": json.work[i].res});
        }          
    });
    return works_info;
}
function update_works(works){
    works = get_works()
}
////////////////////////// RandomGet //////////////////////////
function RandomGet ()
{
    var foo = new Date;
    return foo.getTime();
}
////////////////////////// Listners //////////////////////////
function addOnMessageListener()
{
    chrome.runtime.onMessage.addListener(function (req, sender, sendRes) {
        if(req.type == 'work'){ 
            if(req.msg == 'start'){
                var works_info = get_works();
                var works = req.list;
                var works_info = get_works();
                var works_payd = ["бар", "кг", "кг", "G", "р", "р", "р", "р", "р"];
                setInterval(working, 30000, works, works_info, works_payd);
                setInterval(update_works, 10000, works_info); 
            }
            if(req.msg == 'stop'){
                clearInterval(working);
                clearInterval(update_works);
            } 
        }
        sendRes({
            'collect': true
        });
    });
}
addOnMessageListener();
